#!/bin/bash

# Load some useful functions and nice coloring.
source ./util.sh

function make_and_install()
{
    runcmd ./configure --prefix="$1"
    runcmd make -j4 --silent
    runcmd make install --silent
}

function setup_reproducible_build()
{
    # Shellcheck SC2155
    PWD="$(pwd)"
    readonly BUILD="$PWD/build"
    local PREFIX="$BUILD/prefix"

    local V_AUTOMAKE="1.16"
    local V_AUTOCONF="2.69"
    local V_ASCIIDOC="9.1.1"

    local FNAME_AUTOMAKE="automake-$V_AUTOMAKE"
    local FNAME_AUTOCONF="autoconf-$V_AUTOCONF"
    local FNAME_ASCIIDOC="asciidoc-$V_ASCIIDOC"

    export PATH="$PREFIX/bin:$PATH"

    # Packages needed to build tor. Only do this when root else we get
    # annoying warnings and failures.
    if [[ $EUID -eq 0 ]]; then
        runcmd apt install -y autoconf docbook-xml docbook-xsl gcc \
                              libevent-dev libssl-dev libxml2-utils m4 make \
                              pkg-config xsltproc zlib1g-dev
    fi

    # Import the GNU.org GPG key that signs the packages.
    runcmd gpg --import "$PWD/gpg/autoconf.asc"
    runcmd gpg --import "$PWD/gpg/automake.asc"

    runcmd mkdir -p "$PREFIX"
    runcmd cd "$BUILD"

    runcmd umask 0002

    download https://ftp.gnu.org/gnu/automake/$FNAME_AUTOMAKE.tar.gz
    download https://ftp.gnu.org/gnu/automake/$FNAME_AUTOMAKE.tar.gz.sig
    download https://ftp.gnu.org/gnu/autoconf/$FNAME_AUTOCONF.tar.gz
    download https://ftp.gnu.org/gnu/autoconf/$FNAME_AUTOCONF.tar.gz.sig
    # Unfortunately, no checksums or signature exists for Github packages.
    # This checksum was taken from 3 different sources as in my home
    # connection, over Tor and over a remote server.
    # Also confirmed with: https://www.freshports.org/textproc/asciidoc/
    FNAME_ASCIIDOC_SHA256SUM="ea39760ac2739496c14002902571592dc2ae2fa673296ec141a9e491d9c11fca"
    download https://github.com/asciidoc/asciidoc-py3/releases/download/$V_ASCIIDOC/$FNAME_ASCIIDOC.tar.gz

    # Verify checksum
    DOWNLOADED_ASCIIDOC_CHECKSUM="$(checksum_file "$FNAME_ASCIIDOC.tar.gz" | cut -f1 -d' ')"
    if [ "$FNAME_ASCIIDOC_SHA256SUM" != "$DOWNLOADED_ASCIIDOC_CHECKSUM" ]; then
        die "The asciidoc package doesn't match our checksum $FNAME_ASCIIDOC_SHA256SUM. We got $DOWNLOADED_ASCIIDOC_CHECKSUM"
    fi

    # Verify GPG signatures
    runcmd gpg --verify "$FNAME_AUTOMAKE.tar.gz.sig"
    runcmd gpg --verify "$FNAME_AUTOCONF.tar.gz.sig"

    runcmd tar -xf $FNAME_AUTOMAKE.tar.gz
    runcmd tar -xf $FNAME_AUTOCONF.tar.gz
    runcmd tar -xf $FNAME_ASCIIDOC.tar.gz

    runcmd cd "$FNAME_AUTOCONF/"
    make_and_install "$PREFIX"
    runcmd cd -

    runcmd cd "$FNAME_AUTOMAKE/"
    make_and_install "$PREFIX"
    runcmd cd -

    runcmd cd "$FNAME_ASCIIDOC/"
    make_and_install "$PREFIX"
    runcmd cd -
}

function build_reproducible()
{
    runcmd ./autogen.sh
    runcmd ./configure
    runcmd make -j4 dist-reprod
}

# Setup the environment
setup_reproducible_build

# Get tor repository
clone_tor
# Get into the repository we cloned before.
runcmd cd tor/

# Build all versions found in the version artifacts. The filename is the
# version and its content is the branch.
for file in "$VERSIONS_DIR"/*
do
    # Get version and branch from artifacts
    VERSION=$(basename "$file")
    BRANCH=$(cat "$file")

    announce "Building tarball for tor-$VERSION on branch $BRANCH"

    # Switch version.
    runcmd git switch "$BRANCH"

    # Build the tarball in a reproducible way.
    build_reproducible

    TARBALL_FNAME="tor-$VERSION.tar.gz"

    # Checksum and copy the tarball in the artifact directory.
    echo "$(checksum_file "$TARBALL_FNAME")" > "$TARBALLS_DIR/$TARBALL_FNAME.sha256sum"
    runcmd mv "$TARBALL_FNAME" "$TARBALLS_DIR/"
done

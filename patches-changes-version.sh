#!/bin/bash

# shellcheck disable=SC2129

# Load some useful functions and nice coloring.
source ./util.sh

function construct_changelog()
{
    local changelog="changelog.in"
    local releasenotes="releasenotes.in"
    local VERSION="$1"

    # Build the ChangeLog file
    echo "Changes in version $VERSION - $(date -u +%Y-%m-%d)" > $changelog
    echo "" >> $changelog
    echo "INSERT SUMMARY BLURP" >> $changelog
    echo "" >> $changelog
    ./scripts/maint/sortChanges.py changes/* >> $changelog
    runcmd ./scripts/maint/format_changelog.py --inplace $changelog

    # Put ChangeLog into ReleaseNotes
    head -4 ReleaseNotes > $releasenotes
    cat $changelog >> $releasenotes
    tail -n +5 ReleaseNotes >> $releasenotes
    mv -f $releasenotes ReleaseNotes

    # Construct ChangeLog
    cat ChangeLog >> $changelog
    mv -f $changelog ChangeLog

    # Remove changes file
    rm changes/*
}

# Get tor repository
clone_tor
# Get into the repository we cloned before.
runcmd cd tor/

# Build all versions found in the version artifacts. The filename is the
# version and its content is the branch.
for file in "$VERSIONS_DIR"/*
do
    # Get version and branch from artifacts
    VERSION=$(basename "$file")
    BRANCH=$(cat "$file")

    announce "Constructing changelog for $VERSION from branch $BRANCH"

    # Switch version.
    runcmd git switch "$BRANCH"

    # Apply patches from the preliminary stage because there might be changes
    # files. We only apply the "maint" patches which are the ones that apply
    # to all branches and need to be merge forward. These need to be reflected
    # into the ChangeLog, not the rest.
    for patch in "$PATCHES_DIR"/maint/*
    do
        # Make sure it is a legit file.
        if [ -f "$patch" ]; then
            runcmd git am "$patch"
        fi
    done

    # Build the ChangeLog and ReleaseNotes files.
    construct_changelog "$VERSION"

    # Git add everything and commit.
    runcmd git add -u
    runcmd git commit -a -m "release: ChangeLog and ReleaseNotes for $VERSION"

    # Extract the patch and save it as an artifacts. ChangeLog patch applies
    # _only_ to release branches or main (for alphas).
    runcmd git format-patch -1 -o "$PATCHES_DIR/$BRANCH/"

    # Build the version now
    change_version "$VERSION"

    # Git add everything and commit.
    runcmd git add -u
    runcmd git commit --allow-empty -a -m "version: Bump version to $VERSION"

    # Extract the patch and put it in the artifacts. The version change
    # applies to the corresponding "maint" branch and so get that branch.
    # Remember, an alpha doesn't have a maint branch thus the function call
    # which will return "main" if so.
    runcmd git format-patch -1 -o "$PATCHES_DIR/$(maint_branch "$VERSION" "$BRANCH")"
done

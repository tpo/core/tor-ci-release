#!/bin/bash

# Load some useful functions and variables.
source ./util.sh

# Get the Tor repository. We only need main.
clone_tor --depth=1 -b main
runcmd cd tor/

# Update GeoIP files and commit them to the repository.
runcmd ./scripts/maint/geoip/update_and_commit_geoip.sh

# Extract the patch and put it in the artifacts. This applies to all
# maintained branch and thus why we put it in "maint". It is merge forward.
runcmd git format-patch -1 -o "$PATCHES_DIR/maint"

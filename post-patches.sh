#!/bin/bash

# Load some useful functions and nice coloring.
source ./util.sh

# Get tor repository
clone_tor
# Get into the repository we cloned before.
runcmd cd tor/

# Build all versions found in the version artifacts. The filename is the
# version and its content is the branch.
for file in "$VERSIONS_DIR"/*
do
    # Get version and branch from artifacts
    VERSION=$(basename "$file")
    BRANCH=$(cat "$file")
    # We bump it with -dev
    NEW_VERSION="$VERSION-dev"

    announce "Changing version to $NEW_VERSION on branch $BRANCH"

    # Switch version.
    runcmd git switch "$BRANCH"

    # Build the version now
    change_version "$NEW_VERSION"

    # Git add everything and commit.
    runcmd git add -u
    runcmd git commit -a -m "version: Bump version to $NEW_VERSION"

    # Extract the patch and put it in the artifacts. This is a patch for the
    # specific maint branch and thus is merge -s ours only. We call this
    # function because an alpha will use "main".
    runcmd git format-patch -1 -o "$PATCHES_DIR/$(maint_branch "$NEW_VERSION" "$BRANCH")"
done

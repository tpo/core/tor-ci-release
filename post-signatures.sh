#!/bin/bash

# Load some useful functions and nice coloring.
source ./util.sh

# Get the reproducible repository and go in it.
runcmd git clone https://gitlab.torproject.org/tpo/core/tor-ci-reproducible.git
runcmd cd tor-ci-reproducible/

# Go over each versions.
for file in "$VERSIONS_DIR"/*
do
    # Get version and tarball name we are about to try to sign.
    VERSION=$(basename "$file")
    TARBALL_CHECKSUM_FNAME="tor-$VERSION.tar.gz.sha256sum"

    # For every signature we have, verify it and append if good.
    for sig_file in sigs/"$VERSION"/*
    do
        # This is the crucial step. If the tarball is reproducible, the
        # signature MUST verify the generated tarballs of the CI. If not, the
        # build was not reproducible and we die immediately.
        if ! runcmd gpg --verify "$sig_file" "$TARBALLS_DIR/$TARBALL_CHECKSUM_FNAME"; then
            die "Signature $sig_file failed to GPG verify. Stopping"
        fi
        # Concat the signature.
        cat "$sig_file" >> "$TARBALLS_DIR/$TARBALL_CHECKSUM_FNAME.asc"
        announce "Appending $sig_file to final signature"
    done

    # Validate final team signature.
    if ! runcmd gpg --verify "$TARBALLS_DIR/$TARBALL_CHECKSUM_FNAME.asc" "$TARBALLS_DIR/$TARBALL_CHECKSUM_FNAME"; then
        die "Final team signature failed to GPG verify. Stopping"
    fi

    success "Signature has been created!"
done

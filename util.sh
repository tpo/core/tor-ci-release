#!/usr/bin/env bash

T_RED=$(tput setaf 1 || true)
T_GREEN=$(tput setaf 2 || true)
T_YELLOW=$(tput setaf 3 || true)
T_BOLD=$(tput bold || true)
T_RESET=$(tput sgr0 || true)

BUILD_DIR="${BUILDDIR:-$CI_PROJECT_DIR}"
ARTIFACTS_DIR=$BUILD_DIR/"${ARTIFACTS_DIR:-artifacts}"
PATCHES_DIR=$BUILD_DIR/"${PATCHES_DIR:-patches}"
TARBALLS_DIR=$BUILD_DIR/"${TARBALLS_DIR:-tarballs}"
VERSIONS_DIR=$BUILD_DIR/"${VERSIONS_DIR:-versions}"

function checksum_file()
{
    local file="$1"
    echo "$(sha256sum $file)"
}

function download()
{
    local URL="$1"

    OPTS="-O"
    if [ -n "$2" ]; then
        OPTS="-o $2"
    fi

    echo "Downloading $URL"
    http_code=$(curl -L -w "%{http_code}" $OPTS "$URL")
    if [ "$?" -ne 0 ] || [ "$http_code" -ne 200 ]; then
        die "Failed to download $URL. HTTP code: $http_code. Stopping"
    fi
}

function maint_branch()
{
    local VERSION=$1
    local BRANCH=$2

    if [ "$BRANCH" = "main" ]; then
        echo "$BRANCH"
    else
        echo "maint-$(major_version "$VERSION")"
    fi
}

# Get the major version from a tor version. For example: 0.4.6.7 -> 0.4.6
function major_version()
{
    local VERSION="$1"
    echo "${VERSION%.*}"
}

# Validate tor.git version format. This dies if wrong format.
function validate_version_format()
{
    local VERSION="$1"
    local rx='^0.([0-9]+\.){2}(\*|[0-9]+|([0-9]+)-[a-z-]+)$'
    if [[ ! $VERSION =~ $rx ]]; then
        die "Version format is not valid: \"$VERSION\""
    fi
}

function change_version()
{
    local file="configure.ac"
    local VERSION="$1"

    # Some bash ninji-tsu to change the version line.
    line=$(grep AC_INIT "$file")
    IFS=',' read -r -a ARRAY <<< "$line"
    newline="${ARRAY[0]},[$VERSION])"

    # Replace the line.
    sed -i "s/AC_INIT.*/$newline/g" $file

    runcmd ./scripts/maint/update_versions.py
}

function die()
{
    echo "${T_BOLD}${T_RED}FATAL ERROR:${T_RESET} $*" 1>&2
    exit 1
}

function success()
{
    echo "${T_BOLD}${T_GREEN}SUCCESS:${T_RESET} $*" 1>&2
}


function error()
{
    echo "${T_BOLD}${T_RED}ERROR:${T_RESET} $*" 1>&2
}

function announce()
{
    echo "${T_BOLD}${T_YELLOW}=== $1 ===${T_RESET}" 1>&2
}

function runcmd()
{
    echo "${T_BOLD}${T_GREEN}\$ $*${T_RESET}"
    if ! "$@" ; then
        die "command '$*' has failed."
    fi
}

function clone_arti()
{
    if [ ! -d "arti" ]; then
        runcmd git clone $* https://gitlab.torproject.org/tpo/core/arti.git
    fi
}

# shellcheck disable=SC2119,SC2120
function clone_tor()
{
    if [ ! -d "tor" ]; then
        runcmd git clone $* https://gitlab.torproject.org/tpo/core/tor.git
    fi
}
